﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRCam_Mover_Test : MonoBehaviour
{
    public float moveSpeed;
    public float rotSpeed;

    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(-Vector3.forward * Time.deltaTime * moveSpeed);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(Vector3.right * Time.deltaTime * moveSpeed);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(Vector3.left * Time.deltaTime * moveSpeed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.left * Time.deltaTime * rotSpeed);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Rotate(-Vector3.forward * Time.deltaTime * rotSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.right * Time.deltaTime * rotSpeed);
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Rotate(Vector3.forward * Time.deltaTime * rotSpeed);
        }
    }
}
