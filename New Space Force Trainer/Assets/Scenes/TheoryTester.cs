﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheoryTester : MonoBehaviour
{

    public GameObject point1;
    public GameObject point2;
    public GameObject AnewPoint;
    public LineRenderer mainLine;
    public GameObject aSnap;
    public LineRenderer aLine;
    public GameObject box;
    public GameObject secondBox;

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SnapCube();
        }

    }

    private void Reset()
    {
        point1.transform.position = Vector3.zero;
        point2.transform.position = Vector3.zero;
        AnewPoint.transform.position = Vector3.zero;
        aSnap.transform.position = Vector3.zero;
        mainLine.SetPosition(0, Vector3.zero);
        aLine.SetPosition(0, Vector3.zero);
        mainLine.SetPosition(1, Vector3.zero);
        aLine.SetPosition(1, Vector3.zero);
    }
    private void DrawLinesInitial()
    {
        mainLine.SetPosition(0, point1.transform.position);
        mainLine.SetPosition(1, point2.transform.position);
    }
    private void Calculate()
    {
        Vector3 a = point2.transform.position - point1.transform.position;
        Vector3 b1 = point1.transform.position - AnewPoint.transform.position;
        Vector3 b2 = a;
        Vector3 ab1 = a - b1;
        Vector3 ab2 = new Vector3(b2.x * -1, b2.y * -1, b2.z * -1);
        float t1 = ab1.x + ab1.y + ab1.z;
        float t2 = ab2.x + ab2.y + ab2.z;
        float endT = (float)t1 / t2;
        Vector3 newPosition = new Vector3(point1.transform.position.x + (endT * a.x), point1.transform.position.y + (endT * a.y), point1.transform.position.z + (endT * a.z));
        aSnap.transform.position = newPosition;
        aLine.SetPosition(0, AnewPoint.transform.position);
        aLine.SetPosition(1, newPosition);




    }

    void computeDistance(Vector3 A, Vector3 B, Vector3 C)
    {
        Vector3 d = (C - B) / Vector3.Distance(C, B);
        Vector3 v = A - B;
        float t = Vector3.Dot(v, d);
        Vector3 P = B + t * d;
        Debug.Log(P);
        aSnap.transform.position = P;
        aLine.SetPosition(0, AnewPoint.transform.position);
        aLine.SetPosition(1, aSnap.transform.position);
        Debug.Log(Vector3.Distance(P, A));
    }

    public void SnapCube()
    {
        CapsuleCollider capsuleCollider = this.GetComponent<CapsuleCollider>();
        Collider myCollider = this.GetComponent<Collider>();
        Vector3 colliderCenter = capsuleCollider.center;
        Vector3 B = transform.TransformPoint(colliderCenter + new Vector3(0, (capsuleCollider.height / 2.0f), 0));
        Vector3 C = transform.TransformPoint(colliderCenter + new Vector3(0, (-capsuleCollider.height / 2.0f), 0));
        Vector3 A = box.transform.position;
        Vector3 d = (C - B) / Vector3.Distance(C, B);
        Vector3 v = A - B;
        float t = Vector3.Dot(v, d);
        Vector3 P = B + t * d;
        box.transform.position = P;
        Vector3 pa = P - A;
        secondBox.transform.position = A + 2.0f * pa;
    }
}
