﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class lineMathTest : MonoBehaviour
{

    public Text outputText;
    public Transform q;
    public Transform r;
    public Transform p;
    public Transform e;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            outputText.text = "t = (R - Q)*(Q - P)/(R - Q) * (R-Q)";
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            outputText.text += "\nt = (" + r.position + " - " + q.position + ")*(" + q.position+ "  - " + p.position + ")/(" + r.position + " - " + q.position + ") * (" + r.position + "-" + q.position + ")";
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            outputText.text += "\nt = (" + (r.position-q.position) + ")*(" + (q.position - p.position) + ")/(" + (r.position - q.position) +") * (" + (r.position - q.position) + ")";
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Vector3 vect = (q.position - r.position);
            Vector3 vect2 = vect * -1;
            outputText.text = (Vector3.Cross(vect, vect2)).ToString();
            e.position = vect;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            Debug.Log(Vector3.Distance(new Vector3(7.0f, 11.0f, 2.0f), new Vector3(8.0f, 6.5f, 1.5f)));
            Debug.Log(Vector3.Distance(new Vector3(4.0f, 6.0f, 1.0f), new Vector3(8.0f, 6.5f, 1.5f)));
            Debug.Log(Vector3.Distance(new Vector3(4.0f, 6.0f, 1.0f), new Vector3(7.0f, 11.0f, 2.0f)));
        }

    }
}
