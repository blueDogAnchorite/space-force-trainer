﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using UnityEngine.UI;

public class Astronaut_Locomotion : MonoBehaviour
{
    public enum TetherState { Floating, OneHand, TwoHands, Tethered };
    private TetherState currentTetherState = TetherState.Floating;
    private Transform bodyPoint;
    private Vector3 lastVelocity;
    private Vector3[] prevVelcsLog;
    public int previousVelocitySampleDuration;
    public float pushOffStrength;
    public Transform camTransform;
    public SphereCollider playerCollider;
    public Text debugText;
    private List<Hand> tetheredHands;
    private IEnumerator coroutineTrackingVelocity;

    void Start()
    {
       UnityEngine.XR.InputTracking.Recenter();
        bodyPoint = transform;
        prevVelcsLog = new Vector3[previousVelocitySampleDuration];
        tetheredHands = new List<Hand>();
    }

    public IEnumerator KeepTrackOfRecentVelocity(Hand hand1 = null, Hand hand2 = null)
    {
        for (int i = 0; i < prevVelcsLog.Length; i++)
        {
            prevVelcsLog[i] = Vector3.zero;
        }

        if (currentTetherState == TetherState.Floating)
        {
            Transform trackpoint = (hand1 == null) ? this.transform : hand1.transform;
            Vector3 previousPosition1 = trackpoint.localPosition;
            while (currentTetherState == TetherState.Floating)
            {
                

                for (int i = 0; i < prevVelcsLog.Length; i++)
                {
                    prevVelcsLog[i] = ((trackpoint.localPosition - previousPosition1) / Time.deltaTime) * 0.5f;
                    previousPosition1 = trackpoint.localPosition;
                    i = (i == prevVelcsLog.Length) ? i : 0;
                    yield return null;
                }
            }
        }
           
        if ((currentTetherState == TetherState.OneHand)) 
        {

            Vector3 previousPosition1 = hand1.transform.localPosition;
            while (currentTetherState == TetherState.OneHand)
            {
               
                for (int i = 0; i < prevVelcsLog.Length; i++)
                {
                    prevVelcsLog[i] = (hand1.transform.localPosition - previousPosition1) / Time.deltaTime;
                    previousPosition1 = hand1.transform.localPosition;
                    i = (i == prevVelcsLog.Length) ? i : 0;
                    yield return null;
                }
            }
        }
    }

    public TetherState GetState()
    {
        return currentTetherState;
    }

    public void AddHandTetherPoint(Hand handToAdd)
    {
        if (!tetheredHands.Contains(handToAdd))
        {
            tetheredHands.Add(handToAdd);
            switch (currentTetherState)
            {
                case TetherState.Floating:
                    ChangeState(TetherState.OneHand, handToAdd);
                    break;
                case TetherState.OneHand:
                    ChangeState(TetherState.TwoHands, handToAdd);
                    break;
                default:
                    break;
            }
        }
     
    }

    public void RemoveHandTetherPoint(Hand handToRemove)
    {
        if (tetheredHands.Contains(handToRemove))
        {
            tetheredHands.Remove(handToRemove);

            switch (currentTetherState)
            {
                case TetherState.OneHand:
                    ChangeState(TetherState.Floating);
                    break;
                case TetherState.TwoHands:
                    ChangeState(TetherState.OneHand, handToRemove.otherHand);
                    break;
                default:
                    break;
            }
        }
    }

    private void ChangeState(TetherState newState, Valve.VR.InteractionSystem.Hand hand = null)
    {
        StopAllCoroutines();
        switch (newState)
        {
            case TetherState.Floating:
                currentTetherState = TetherState.Floating;
                Float();
                break;
            case TetherState.OneHand:
                currentTetherState = TetherState.OneHand;
                StartCoroutine(OneHand(hand));
                //StartCoroutine(KeepTrackOfRecentVelocity(hand));
                if (coroutineTrackingVelocity != null)
                {
                    StopCoroutine(coroutineTrackingVelocity);
                }
                
                coroutineTrackingVelocity = KeepTrackOfRecentVelocity(hand);
                StartCoroutine(coroutineTrackingVelocity);
                break;
            case TetherState.TwoHands:
                StartCoroutine(TwoHands(hand));
                currentTetherState = TetherState.TwoHands;
                //StartCoroutine(KeepTrackOfRecentVelocity());
                if (coroutineTrackingVelocity != null)
                {
                    StopCoroutine(coroutineTrackingVelocity);
                }
                coroutineTrackingVelocity = KeepTrackOfRecentVelocity();
                StartCoroutine(coroutineTrackingVelocity);
                break;
            case TetherState.Tethered:
                StartCoroutine(Tethered());
                currentTetherState = TetherState.Tethered;
                break;
            default:
                break;
        }
    }
    
    Vector3 GetAveragePrevVel()
    {
        int numOfSamps = 0;
        Vector3 totVel = Vector3.zero;
        for (int i = 0; i < prevVelcsLog.Length; i++)
        {
            if (prevVelcsLog[i] != Vector3.zero)
            {
                numOfSamps++;
                totVel += prevVelcsLog[i];
            }
        }
        return (numOfSamps > 0) ? (totVel / numOfSamps) : Vector3.zero;
    }

    private void Float()
    {
        StopAllCoroutines();
        //ApplyTrajectoryForce();
    }

    public void StartTrackingVelocity(Hand hand1 = null, Hand hand2 = null)
    {
        if (hand1)
        {
            debugText.text = " " + hand1.ToString();
        }
        if (hand2)
        {
            debugText.text = " " + hand2.ToString();
        }
        switch (hand2)
        {
            case null:
                switch (hand1)
                {
                    case null:
                        //StartCoroutine(KeepTrackOfRecentVelocity());
                        if (coroutineTrackingVelocity != null)
                        {
                            StopCoroutine(coroutineTrackingVelocity);
                        }
                        coroutineTrackingVelocity = KeepTrackOfRecentVelocity();
                        StartCoroutine(coroutineTrackingVelocity);
                        break;
                    default:
                        //StartCoroutine(KeepTrackOfRecentVelocity(hand1));
                        if (coroutineTrackingVelocity != null)
                        {
                            StopCoroutine(coroutineTrackingVelocity);
                        }
                        coroutineTrackingVelocity = KeepTrackOfRecentVelocity(hand1);
                        StartCoroutine(coroutineTrackingVelocity);
                        break;
                }
                break;
            default:
                //StartCoroutine(KeepTrackOfRecentVelocity(hand1, hand2));
                if (coroutineTrackingVelocity != null)
                {
                    StopCoroutine(coroutineTrackingVelocity);
                }
                coroutineTrackingVelocity = KeepTrackOfRecentVelocity(hand1, hand2);
                StartCoroutine(coroutineTrackingVelocity);
                break;
        }

    }

    public void StopTrackingVelocity(Hand hand1 = null, Hand hand2 = null)
    {
        //StopCoroutine(coroutineTrackingVelocity);
        StopAllCoroutines();

    }

    public void ApplyTrajectoryForce()
    {
        Rigidbody astroRigidbody = GetComponent<Rigidbody>();
        astroRigidbody.velocity = Vector3.zero;
        astroRigidbody.angularVelocity = Vector3.zero;
        astroRigidbody.AddRelativeForce((new Vector3(-GetAveragePrevVel().x, -GetAveragePrevVel().y, -GetAveragePrevVel().z) * pushOffStrength), ForceMode.Impulse);
    }



    void Update()
    {     
        playerCollider.center = camTransform.localPosition;
        if (Input.GetKeyDown(KeyCode.F))
        {
            StopCoroutine(KeepTrackOfRecentVelocity());
        }
    }

    IEnumerator OneHand(Hand hand)
    {
        Vector3 originalBodyPosition = bodyPoint.position;
        Quaternion previousRot = hand.transform.localRotation;
        Vector3 initialVector = hand.transform.position - originalBodyPosition;
        while (currentTetherState == TetherState.OneHand)
        {
            Quaternion currRot = hand.transform.localRotation;
            Quaternion angularDisp = previousRot * Quaternion.Inverse(currRot);
            Quaternion newRot = bodyPoint.localRotation * angularDisp;
            bodyPoint.localRotation = newRot;
            Vector3 newHandBodyVector = hand.transform.position - bodyPoint.position;
            Vector3 handyBodyVectorDifference = newHandBodyVector - initialVector;
            bodyPoint.position = originalBodyPosition - handyBodyVectorDifference;
            previousRot = hand.transform.localRotation;
            yield return null;
        }
    }

    IEnumerator TwoHands(Hand hand)
    {
        currentTetherState = TetherState.TwoHands;    

        Vector3 originalBodyPosition = bodyPoint.position;
        Quaternion hand1_previousRot = hand.transform.localRotation;
        Vector3 hand1_initialVector = hand.transform.position - originalBodyPosition;
        Quaternion hand2_previousRot = hand.otherHand.transform.localRotation;
        Vector3 hand2_initialVector = hand.otherHand.transform.position - originalBodyPosition;
        Vector3 hand1prevPos = hand.transform.position;
        Vector3 hand2prevPos = hand.otherHand.transform.position;

        while (currentTetherState == TetherState.TwoHands)
        {
            Quaternion hand1_currRot = hand.transform.localRotation;
            Quaternion hand2_currRot = hand.otherHand.transform.localRotation;
            Quaternion hand1_angularDisp = hand1_previousRot * Quaternion.Inverse(hand1_currRot);
            Quaternion hand2_angularDisp = hand2_previousRot * Quaternion.Inverse(hand2_currRot);
            Quaternion middleAngularDisp = Quaternion.Slerp(hand1_angularDisp, hand2_angularDisp, 0.5f);
            Quaternion newRot = bodyPoint.localRotation * middleAngularDisp;
            //bodyPoint.localRotation = newRot;
            Vector3 newHand1BodyVector = hand.transform.position - bodyPoint.position;
            Vector3 newHand2BodyVector = hand.otherHand.transform.position - bodyPoint.position;
            Vector3 avgHandBodyVector = (newHand1BodyVector + newHand2BodyVector) / 2.0f;
            Vector3 handyBodyVectorDifference = avgHandBodyVector - ((hand1_initialVector + hand2_initialVector / 2.0f));
            Vector3 avgHandDisp = -(((hand.transform.position - hand1prevPos) + (hand.otherHand.transform.position - hand2prevPos)) / 2.0f);

            bodyPoint.transform.position += avgHandDisp;
            hand1prevPos = hand.transform.position;
            hand2prevPos = hand.otherHand.transform.position;



            hand1_previousRot = hand.transform.localRotation;
            hand2_previousRot = hand.otherHand.transform.localRotation;
            yield return null;
        }
    }

    IEnumerator Tethered()
    {
        yield return null;
    }
    




    
}
