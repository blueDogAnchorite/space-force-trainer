﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using UnityEngine.UI;


public class Astronaut_Hand : MonoBehaviour
{

    private List<GameObject> objectsCurrentlyCollidingWith;
    private Hand thisHand;
    private SphereCollider handCollider;
    public SteamVR_Action_Boolean grabAction;
    public SteamVR_Input_Sources handSide;
    bool isPushingOff = false;
    public Text debugText;
    private Vector3 originalPalmLocalPosition;
    private Vector3 originalPalmLocalRotation;
    private Vector3 originalModelPosition;
    private Vector3 originalModelRotation;
    private Transform wristPointer;
    private Transform handCenter;


    private void Start()
    {
        objectsCurrentlyCollidingWith = new List<GameObject>();
        thisHand = GetComponent<Hand>();
        if (!thisHand)
        {
            this.gameObject.AddComponent<Hand>();
        }
        handCollider = GetComponent<SphereCollider>();
        if (!handCollider)
        {
            this.gameObject.AddComponent<SphereCollider>();
        }
        grabAction.AddOnStateDownListener(PushOffOn,handSide);
        grabAction.AddOnStateUpListener(PushOffOff, handSide);
        originalPalmLocalPosition = transform.Find("Palm_Center_Point").localPosition;
        originalPalmLocalRotation = transform.Find("Palm_Center_Point").localEulerAngles;
        StartCoroutine(ParentHandModelToPalmCenter());
       


    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("playing");
            AudioSource aso = this.GetComponent<AudioSource>();
            aso.Play(0);
        }
    }

    IEnumerator ParentHandModelToPalmCenter()
    {
        yield return new WaitForSeconds(1.0f);
        GameObject handModel = null;
        while (!handModel)
        {
            if (!handModel)
            {
               // Debug.Log(gameObject.name.Split('H')[0] + "RenderModel(Clone)");
                Transform thandModel = transform.Find((gameObject.name.Split('H')[0] + "RenderModel(Clone)"));
                if (thandModel)
                {
                    handModel = thandModel.gameObject;
                    handModel.transform.SetParent(this.transform.Find("Palm_Center_Point"));
                    originalModelPosition = this.transform.Find("Palm_Center_Point").transform.Find((gameObject.name.Split('H')[0] + "RenderModel(Clone)")).localPosition;
                    originalModelRotation = transform.Find("Palm_Center_Point").transform.Find((gameObject.name.Split('H')[0] + "RenderModel(Clone)")).localEulerAngles;
                    handCenter = Instantiate(new GameObject("hand_center_" + gameObject.name.Split('H')[0]), thandModel.position, thandModel.rotation).transform;
                    
                    handCenter.parent = thandModel;
                    handCenter.localPosition = new Vector3(0.0148f, -0.0052f, -0.1049f);
                    handCenter.localEulerAngles = new Vector3(27.778f, -52.984f, -133.275f);
                    wristPointer = Instantiate(new GameObject("wrist_pointer_" + gameObject.name.Split('H')[0]), thandModel.position, thandModel.rotation).transform;
                    wristPointer.parent = this.transform;
                    if (handSide == SteamVR_Input_Sources.RightHand)
                    {
                        wristPointer.localPosition = new Vector3(0.03034386f, 0.04792903f, -0.1801057f);
                        wristPointer.localEulerAngles = new Vector3(41.75f, 1.601f, -75.12601f);
                    }
                    else
                    {
                        wristPointer.localPosition = new Vector3(-0.02769981f, 0.05049983f, -0.1850003f);
                        wristPointer.localEulerAngles = new Vector3(190.39f, -261.684f, 50.978f);
                    }

                }
            }
            else
            {
                handModel.transform.SetParent(transform.Find("Palm_Center_Point"));
                originalModelPosition = this.transform.Find("Palm_Center_Point").transform.Find((gameObject.name.Split('H')[0] + "RenderModel(Clone)")).localPosition;
                originalModelRotation = transform.Find("Palm_Center_Point").transform.Find((gameObject.name.Split('H')[0] + "RenderModel(Clone)")).localEulerAngles;
                handCenter = Instantiate(new GameObject("hand_center_" + gameObject.name.Split('H')[0]), handModel.transform.position, handModel.transform.rotation).transform;
                handCenter.parent = handModel.transform;
                handCenter.localPosition = new Vector3(0.0148f, -0.0052f, -0.1049f);
                handCenter.localEulerAngles = new Vector3(27.778f, -52.984f, -133.275f);
                wristPointer = Instantiate(new GameObject("wrist_pointer_" + gameObject.name.Split('H')[0]), Vector3.zero, Quaternion.identity).transform;
                wristPointer.parent = this.transform;
                if (handSide == SteamVR_Input_Sources.RightHand)
                {
                    wristPointer.localPosition = new Vector3(0.03034386f, 0.04792903f, -0.1801057f);
                    wristPointer.localEulerAngles = new Vector3(41.75f, 1.601f, -75.12601f);
                }
                else
                {
                    wristPointer.localPosition = new Vector3(-0.02769981f, 0.05049983f, -0.1850003f);
                    wristPointer.localEulerAngles = new Vector3(190.39f, -261.684f, 50.978f);
                }
            }
            yield return new WaitForSeconds(1.0f);
        }

       
    }

    public void ResetPalmRelativeToController()
    {

        transform.Find("Palm_Center_Point").localPosition = originalPalmLocalPosition;
        transform.Find("Palm_Center_Point").localEulerAngles = originalPalmLocalRotation;


        transform.Find("Palm_Center_Point").transform.Find((gameObject.name.Split('H')[0] + "RenderModel(Clone)")).localEulerAngles = originalModelRotation;
        transform.Find("Palm_Center_Point").transform.Find((gameObject.name.Split('H')[0] + "RenderModel(Clone)")).localPosition = originalModelPosition;



    }

    private void PushOffOn(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        if (!IsGrippingInteractable())
        {

            debugText.text = "pushoffON called " + this.GetComponent<Hand>().handType +  "is gripping interactable is " + IsGrippingInteractable();

            GameObject.FindObjectOfType<Astronaut_Locomotion>().StopTrackingVelocity();
            GameObject.FindObjectOfType<Astronaut_Locomotion>().StartTrackingVelocity(this.GetComponent<Hand>());
            handCollider.isTrigger = true;
            isPushingOff = true;
        }

    }
    private void PushOffOff(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        //debugText.text = "pushoffOFF called " + this.GetComponent<Hand>().handType;

        if (FindObjectOfType<Astronaut_Locomotion>().GetState() == Astronaut_Locomotion.TetherState.Floating)
        {
            GameObject.FindObjectOfType<Astronaut_Locomotion>().StopTrackingVelocity(this.GetComponent<Hand>());
            isPushingOff = false;
        }
        
           // handCollider.isTrigger = false;
    }

    

    public void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "notwall")
        {
            return;
        }
        if (!IsGrippingInteractable())
            {
            Debug.Log("is gripping interactable returned false");
        }
            if ((other.tag == "notwall") && !IsGrippingInteractable())
        {
            GameObject.FindObjectOfType<Astronaut_Locomotion>().StartTrackingVelocity(this.GetComponent<Hand>());
        }
        objectsCurrentlyCollidingWith.Add(other.gameObject);
        

    }
    public void OnTriggerStay(Collider other)
    {
        if (other.tag == "wall")
        {
            if (isPushingOff && !IsGrippingInteractable())
            {
                Debug.Log("is pushing off and is not gripping interactable (on trigger stay");
                GameObject.FindObjectOfType<Astronaut_Locomotion>().ApplyTrajectoryForce();
            }
        }
    }

    public void OnTriggerExit (Collider other)
    {
        objectsCurrentlyCollidingWith.Remove(other.gameObject);
        if (other.tag == "notwall")
        {
            GameObject.FindObjectOfType<Astronaut_Locomotion>().StopTrackingVelocity();
        }
       if (other.gameObject.tag == "handle")
        {
            
        }
    }
    

    private bool IsGrippingInteractable()
    {
        Astronaut_Locomotion astroLoco = FindObjectOfType<Astronaut_Locomotion>();
        if (astroLoco)
        {
            if (astroLoco.GetState() == Astronaut_Locomotion.TetherState.OneHand || astroLoco.GetState() == Astronaut_Locomotion.TetherState.TwoHands)
            {
                Debug.Log(astroLoco.GetState());
                return true;
            }
            else
            {
                Debug.Log(astroLoco.GetState());
                return false;
            }
        }
        else
        {
            return true;
        }
        /*
        Debug.Log("is gripping interactable called");
        string list = "";
        foreach (GameObject item in objectsCurrentlyCollidingWith)
        {
            list += " " + item.name + ":" + handSide;
  
        }
        foreach (GameObject item in objectsCurrentlyCollidingWith)
        {
            if (item.tag == "handle")
            {
                Debug.Log("is gripping interactable returned true" + list);
                return true;
            }
        }
        foreach (GameObject item in GetComponent<Hand>().otherHand.transform.GetComponent<Astronaut_Hand>().objectsCurrentlyCollidingWith)
        {
            list += " " + item.name + ":" + handSide;
        }
        foreach (GameObject item in GetComponent<Hand>().otherHand.transform.GetComponent<Astronaut_Hand>().objectsCurrentlyCollidingWith)
        {
            if (item.tag == "handle")
            {
                Debug.Log("is gripping interactable returned true" + list);
                return true;

            }
        }
        Debug.Log("is gripping interactable returned false, " + list);
        return false;
            */
    }

    public void SetHandModelPositionRotation(Vector3 position, Vector3 rotation)
    {
        GameObject palmCenterPoint = transform.Find("Palm_Center_Point").gameObject;
        if (!palmCenterPoint) { Debug.Log("palm center point not found"); }
        Transform model = transform.Find("Palm_Center_Point").transform.Find((gameObject.name.Split('H')[0] + "RenderModel(Clone)"));
        if (!model)
        { Debug.Log((gameObject.name.Split('H')[0] + "RenderModel(Clone)") + " not found"); }
        transform.Find("Palm_Center_Point").transform.Find((gameObject.name.Split('H')[0] + "RenderModel(Clone)")).localEulerAngles = rotation;
        transform.Find("Palm_Center_Point").transform.Find((gameObject.name.Split('H')[0] + "RenderModel(Clone)")).localPosition = position;
    }


}


