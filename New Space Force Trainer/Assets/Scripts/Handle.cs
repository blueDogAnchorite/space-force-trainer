﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using UnityEngine.UI;

public class Handle : MonoBehaviour
{

    private Astronaut_Locomotion astroLoco;
    private List<Hand> handsHoldingHandle;
    private List<Hand> handsTouchingHandle;
    public SteamVR_Input_Sources handType;
    public SteamVR_Action_Boolean grabAction;
    public Text debugText;
    public Vector3 handGripLookatPoint = Vector3.zero;
    private Vector3 handModelGripDisplacementPosition = new Vector3(43.675f, -188.071f, -198.402f);
    private Vector3 handModelGripDisplacementRotation = new Vector3(0.0196f, -0.0118f, -0.0608f);

    List<GameObject> markers;


    void Start()
    {
        markers = new List<GameObject>();
        handsHoldingHandle = new List<Hand>();
        handsTouchingHandle = new List<Hand>();
        astroLoco = (Astronaut_Locomotion)FindObjectOfType(typeof(Astronaut_Locomotion));
        debugText = GameObject.FindObjectOfType<Text>();
    }

    public bool GetGrab()
    {
        return grabAction.GetState(handType);
    }

    private void OnTriggerEnter(Collider other)
    {
       if (other.GetComponent<Hand>())
        {
            handsTouchingHandle.Add(other.GetComponent<Hand>());
            StartCoroutine(WatchForHandSqueeze(other.GetComponent<Hand>()));
        }
    }

    IEnumerator WatchForHandSqueeze(Hand hand)
    {
        SteamVR_Input_Sources handTypeForCheck = hand.handType;
        
        while (true)
        {
            if (grabAction.GetState(hand.handType))
            {
                astroLoco.StopTrackingVelocity();
                SnapHandToHandle(hand);
                handsHoldingHandle.Add(hand);
                if (!astroLoco) { Debug.Log("Astronaut_Locomotion script not found"); }
                astroLoco.AddHandTetherPoint(hand);
                StartCoroutine(WatchForLetGo(hand));
                break;

            }
            yield return null;
        }
    }

    IEnumerator WatchForLetGo(Hand hand)
    {

        while (grabAction.GetState(hand.handType))
        {
            yield return null;
        }
        GameObject.FindObjectOfType<Astronaut_Locomotion>().ApplyTrajectoryForce();
        hand.transform.GetComponent<Astronaut_Hand>().ResetPalmRelativeToController();
        ResetHandModelPosition(hand);
        handsHoldingHandle.Remove(hand);
        astroLoco.RemoveHandTetherPoint(hand);
        StartCoroutine(WatchForHandSqueeze(hand));


    }
    
        private void OnTriggerExit(Collider other)
    { 
        if (other.GetComponent<Hand>())
        {
            
            StopAllCoroutines();
            handsTouchingHandle.Remove(other.GetComponent<Hand>());
            handsHoldingHandle.Remove(other.GetComponent<Hand>());
            if (handsHoldingHandle.Count > 0)
            {
                StartCoroutine(WatchForLetGo(other.GetComponent<Hand>().otherHand));
            }
            else if (handsTouchingHandle.Count > 0)
            {
                StartCoroutine(WatchForHandSqueeze(other.GetComponent<Hand>().otherHand));
            }
            astroLoco.RemoveHandTetherPoint(other.GetComponentInParent<Hand>());

        }

    }


    void ResetHandModelPosition(Hand hand)
    {

        hand.GetComponent<Astronaut_Hand>().ResetPalmRelativeToController();
    }

    Vector3 HandleUpPositionForPositioningGrip(Hand hand)
    {
        Transform glove = hand.transform.Find("Palm_Center_Point").transform.Find((hand.name.Split('H')[0] + "RenderModel(Clone)"));
        Transform gloveCenter = glove.Find("hand_center_" + hand.name.Split('H')[0] + "(Clone)");
        Transform wristPoint = hand.transform.Find("wrist_pointer_" + hand.name.Split('H')[0] + "(Clone)");
        Vector3 topHandleColliderPoint = transform.TransformPoint(GetComponent<CapsuleCollider>().center + new Vector3(0, GetComponent<CapsuleCollider>().height / 2.0f, 0));
        Vector3 bottomHandleColliderPoint = transform.TransformPoint(GetComponent<CapsuleCollider>().center - new Vector3(0, GetComponent<CapsuleCollider>().height / 2.0f, 0));
        Vector3 vectorForGloveLookAtUp = (gloveCenter.transform.InverseTransformPoint(topHandleColliderPoint).x >= gloveCenter.transform.InverseTransformPoint(bottomHandleColliderPoint).x) ? transform.up : -transform.up;
        Vector3 rightestPoint = (gloveCenter.transform.InverseTransformPoint(topHandleColliderPoint).x >= gloveCenter.transform.InverseTransformPoint(bottomHandleColliderPoint).x) ? (topHandleColliderPoint) : (bottomHandleColliderPoint);
        //vectorForGloveLookAtUp = (hand.handType == SteamVR_Input_Sources.LeftHand) ? -vectorForGloveLookAtUp : vectorForGloveLookAtUp;
        return vectorForGloveLookAtUp;

    }

    Vector3 HandGripSnapPosition(Hand hand)
    {
        Vector3 SnapPoint = Vector3.zero;
        CapsuleCollider capsuleCollider = this.GetComponent<CapsuleCollider>();
        Collider myCollider = this.GetComponent<Collider>();
        Vector3 colliderCenter = capsuleCollider.center;
        Vector3 B = transform.TransformPoint(colliderCenter + new Vector3(0, (capsuleCollider.height / 2.0f), 0));
        Vector3 C = transform.TransformPoint(colliderCenter + new Vector3(0, (-capsuleCollider.height / 2.0f), 0));
        Vector3 A = hand.transform.position;
        Vector3 d = (C - B) / Vector3.Distance(C, B);
        Vector3 v = A - B;
        float t = Vector3.Dot(v, d);
        Vector3 P = B + t * d;
        SnapPoint = P;
        Vector3 pa = P - A;
        //handGripLookatPoint = (A + (10.0f * pa));
        CalculateHandGripLookAtPoint(hand);
        return SnapPoint;
    }

    void CalculateHandGripLookAtPoint(Hand hand)
    {
        Vector3 SnapPoint = Vector3.zero;
        CapsuleCollider capsuleCollider = this.GetComponent<CapsuleCollider>();
        Collider myCollider = this.GetComponent<Collider>();
        Transform wristPoint = hand.transform.Find("wrist_pointer_" + hand.name.Split('H')[0] + "(Clone)");
        Vector3 colliderCenter = capsuleCollider.center;
        Vector3 B = transform.TransformPoint(colliderCenter + new Vector3(0, (capsuleCollider.height / 2.0f), 0));
        Vector3 C = transform.TransformPoint(colliderCenter + new Vector3(0, (-capsuleCollider.height / 2.0f), 0));
        Vector3 A = wristPoint.position;
        Vector3 d = (C - B) / Vector3.Distance(C, B);
        Vector3 v = A - B;
        float t = Vector3.Dot(v, d);
        Vector3 P = B + t * d;
        SnapPoint = P;
        Vector3 pa = P - A;
        handGripLookatPoint = (A - (10.0f * pa));
    }

    void SnapHandToHandle(Hand hand)
    {
        hand.transform.Find("Palm_Center_Point").transform.position = HandGripSnapPosition(hand);
        hand.transform.Find("Palm_Center_Point").transform.LookAt(handGripLookatPoint, HandleUpPositionForPositioningGrip(hand));
        hand.GetComponent<Astronaut_Hand>().SetHandModelPositionRotation(new Vector3(0.0196f, -0.0118f, -0.0608f), new Vector3(43.675f, -188.071f, -198.402f));
    }



}
