﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using UnityEngine.UI;

public class ExperimentalHandLock : MonoBehaviour
{

    private Astronaut_Locomotion astroLoco;
    private List<Hand> handsHoldingHandle;
    private List<Hand> handsTouchingHandle;
    public SteamVR_Input_Sources handType;
    public SteamVR_Action_Boolean grabAction;
    public Text debugText;
    public GameObject testSphere;

    void Start()
    {
        handsHoldingHandle = new List<Hand>();
        handsTouchingHandle = new List<Hand>();
        astroLoco = (Astronaut_Locomotion)FindObjectOfType(typeof(Astronaut_Locomotion));
        debugText = GameObject.FindObjectOfType<Text>();
    }

    public bool GetGrab()
    {
        return grabAction.GetState(handType);
    }

    private void Update()
    {
        //debugText.text = handsTouchingHandle.Count.ToString()
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Hand>())
        {

            handsTouchingHandle.Add(other.GetComponent<Hand>());
            StartCoroutine(WatchForHandSqueeze(other.GetComponent<Hand>()));
            //debugText.text = handsTouchingHandle.Count.ToString() + " hands touching handle \n " + handsHoldingHandle.Count.ToString() + " hands gripping handle";
        }
    }

    IEnumerator WatchForHandSqueeze(Hand hand)
    {
        SteamVR_Input_Sources handTypeForCheck = hand.handType;

        while (true)
        {
            if (grabAction.GetState(hand.handType))
            {

                handsHoldingHandle.Add(hand);
                astroLoco.AddHandTetherPoint(hand);
                StartCoroutine(WatchForLetGo(hand));
                StartCoroutine(DoThingToHand(hand));
                //debugText.text = handsTouchingHandle.Count.ToString() + " hands touching handle \n " + handsHoldingHandle.Count.ToString() + " hands gripping handle";
                break;

            }
            yield return null;
        }
    }

    IEnumerator WatchForLetGo(Hand hand)
    {

        while (grabAction.GetState(hand.handType))
        {
            yield return null;
        }

        handsHoldingHandle.Remove(hand);
        astroLoco.RemoveHandTetherPoint(hand);
        //debugText.text = handsTouchingHandle.Count.ToString() + " hands touching handle \n " + handsHoldingHandle.Count.ToString() + " hands gripping handle";
        StopAllCoroutines();
        StartCoroutine(WatchForHandSqueeze(hand));
        //debugText.text += "\n " + hand.handType + " let go";
        // debugText.text += "\n " + handsTouchingHandle.Count.ToString() + " hands touching handle";

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Hand>())
        {

            StopAllCoroutines();
            handsTouchingHandle.Remove(other.GetComponent<Hand>());

            handsHoldingHandle.Remove(other.GetComponent<Hand>());
            if (handsHoldingHandle.Count > 0)
            {
                StartCoroutine(WatchForLetGo(other.GetComponent<Hand>().otherHand));
            }
            else if (handsTouchingHandle.Count > 0)
            {
                StartCoroutine(WatchForHandSqueeze(other.GetComponent<Hand>().otherHand));
            }
            //debugText.text = handsTouchingHandle.Count.ToString() + " hands touching handle \n "+ handsHoldingHandle.Count.ToString() + " hands gripping handle";
            astroLoco.RemoveHandTetherPoint(other.GetComponentInParent<Hand>());
            //debugText.text += "\n " + other.GetComponent<Hand>().handType + " exited triger";
            // debugText.text += "\n " + handsTouchingHandle.Count.ToString() + " hands touching handle";
        }

    }

    IEnumerator DoThingToHand(Hand hand)
    {
        while (true)
        {
            SphereCollider handCollider = hand.GetComponent<SphereCollider>();
            Vector3 originalHandColliderDisplacement = handCollider.center;
            Vector3 newHandPosition = hand.transform.InverseTransformPoint(this.transform.position);// + originalHandColliderDisplacement;
            testSphere.transform.localPosition = newHandPosition;
            Debug.Log(hand.transform.position);
            yield return null;
        }
    }




}
