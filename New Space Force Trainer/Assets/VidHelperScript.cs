﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VidHelperScript : MonoBehaviour { 

    public float speed;
    public float rotationSpeed;

    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.localPosition += Vector3.forward * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.localPosition += Vector3.left * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.localPosition += -Vector3.forward * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.localPosition += Vector3.right * speed * Time.deltaTime;
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            SceneManager.LoadScene(0);
        }
        if (Input.GetKey(KeyCode.J))
        {
            transform.localEulerAngles += Vector3.up * rotationSpeed * Time.deltaTime;
        }

    }
}
